# PROCÉDURE D'UTILISATION DE GIT POUR LE PROJET

## À faire régulièrement (1 fois par merge des collèges voir 1 fois par jours) :

- Pour mettre à jour le dépôt local par rapport aux travaux implémentés sur GitLab.
###
    git pull

## Toujours vérifier l'emplacement du terminal pour la gestion du projet :

- Des erreurs peuvent survenir si le terminal n'est pas dans le dossier racine du projet (par exemple ne pas voir des fichiers modifiés) :

###
    ls 

- Les dossiers Algorithme, Cahier des charges, les fichiers .gitignore, .gitattributes doivent apparaitres. Les autres dosiers et fichiers doivent apparaitres en fonction de leurs états d'avancement.

## Une fonctionnalité = Une branche avec un nom explicite

- Quand un fonctionnalité va commencer, il faut créer une branche lui correspondant :
###
    git branch <nom de la branche>
    ou
    git checkout -b <nom de la branche>

## Faire son commit :

### Vérifier l'état de sa branche

- Avant tout commit, il est de bonne pratique de vérifier l'état des fichiers:
####
    git status

### Ajout des fichiers modifiés

- Ajouter les fichiers à un commit si et seulement si ils sont dépendants entre eux et nécessaire pour fonctionner : Sinon créer des commit différents et faire des trackage de fichiers différentiés.
####
    git add <nom du fichier>

### Création du commit

- Un commit est de rigueur lorsqu'un fichier ou groupe de fichier est terminé (au moins dans une version fonctionnelle). Si le fichier doit être retoucher plus tard, il doit quand même être inclue.
- Ne pas poublier à pull la branche en cas de modification par une autre personne.
####
    git pull
    git commit -m "message à noté pour justifier le commit"

- L'utilisation des gitmojis pour décrire l'état du code committé est conseillé mais pas obligatoire : https://gitmoji.dev/

### Récupérer le dépôt pour vérifier au cas où d'autres branches auraient été merge. 
- Les modifications ont été faite avec un code qui ne prend pas en compte les modifications des autres, penser à vérifier le nouveau fonctionnement :
###
    git rebase main

### Push sur le dépôt distant
- Lorsqu'un commit est prêt il doit être push le plus tôt possible sur le dépôt pour être disponible à tous :
####
    git push origin <mabranche>
- Si la branche n'est pas encore créée sur le dépôt distant :
####
    git push --set-upstream <nom de la branch>

### Charger sa branche sur le main
- Quand tout est vérifié, il faut charger ses données sur le main :
####
    git checkout main
    git merge <nom de sa branche>

### Vérifier sur le graphe si la branche pointe sur le main.

### !!!!!!!! Recréer une nouvelle branche de travail !!!!!!