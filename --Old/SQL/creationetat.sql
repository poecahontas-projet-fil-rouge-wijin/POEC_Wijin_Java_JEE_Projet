/* Creation de la table Etat */

create table etat(
	id_etat int,
    etat varchar(255)
);
alter table etat
	add constraint id_etat_pk
	primary key (id_etat)	
;

/* Creation des Etats */

insert into etat (
    id_etat,etat)
values
        (0, 'lead'),
        (1, 'client')
;