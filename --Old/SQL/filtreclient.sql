/* Affichage Lead */
select 
    c.entreprise as Entreprise,
    concat(lower(c.prenom), ' ', upper(c.nom))as Contact,
    e.etat as "Client / Lead"
from
    client c,
    etat e
where 
    c.etat = 1
    and c.etat = e.id_etat
order by c.entreprise
;