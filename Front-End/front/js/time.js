function afficherHeure() {
    let date = new Date();
    let heure = date.getHours();
    let minutes = date.getMinutes();
    let secondes = date.getSeconds();

    // Formatage de l'heure pour avoir un affichage plus agréable
    let heureFormattee = heure < 10 ? "0" + heure : heure;
    let minutesFormattees = minutes < 10 ? "0" + minutes : minutes;
    let secondesFormattees = secondes < 10 ? "0" + secondes : secondes;

    let heureActuelle = heureFormattee + ":" + minutesFormattees;  // + ":" + secondesFormattees

    document.getElementById("heure").innerHTML = heureActuelle;
}

// Mettre à jour l'heure chaque seconde
setInterval(afficherHeure, 1000);

// Appel initial pour afficher l'heure immédiatement au chargement de la page
afficherHeure();

function afficherDate() {
    let date = new Date();

    // Obtenir le jour, le mois et l'année
    let jour = date.getDate();
    let mois = date.getMonth() + 1; // Les mois commencent à partir de zéro, donc on ajoute 1
    let anneeFormattee = date.getFullYear();

    // Formatage de la date pour l'affichage
    let jourFormattee = jour < 10 ? "0" + jour : jour;
    switch(mois){
        case 1 :
            mois = "Janvier";
            break;
        case 2 :
            mois = "Février";
            break;
        case 3 :
            mois = "Mars";
            break;
        case 4 :
            mois = "Avril";
            break;
        case 5:
            mois = "Mai";
            break;
        case 6:
            mois = "Juin";
            break;
        case 7:
            mois = "Juillet";
            break;
        case 8:
            mois = "Aout";
            break;
        case 9:
            mois = "Septembre";
            break;
        case 10:
            mois = "Octobre";
            break;
        case 11:
            mois = "Novembre";
            break;
        case 12:
            mois = "Décembre";
            break;
        default:
            mois = "erreur";
    }

    let dateActuelle = jourFormattee + " " + mois + " " + anneeFormattee;

    document.getElementById("date").innerHTML = dateActuelle;
}
setInterval(afficherDate, 1000);
// Appel initial pour afficher la date immédiatement au chargement de la page
afficherDate();