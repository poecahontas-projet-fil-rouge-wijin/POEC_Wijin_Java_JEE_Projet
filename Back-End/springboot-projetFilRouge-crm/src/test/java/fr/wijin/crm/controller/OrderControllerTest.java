package fr.wijin.crm.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import fr.wijin.crm.SpringbootProjetFilRougeCrmApplication;
import fr.wijin.crm.form.OrderForm;
import jakarta.servlet.http.HttpSession;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {SpringbootProjetFilRougeCrmApplication.class}, loader = AnnotationConfigContextLoader.class)
@DirtiesContext
public class OrderControllerTest {
    
    @Autowired
    private OrderController orderController;

    @Mock
    HttpSession mockSession;

    @Mock
    Model mockModel;

    @Mock
    OrderForm mockForm;

    @Mock
    BindingResult mockBindingResult;

    @Test
    void testWelcomeWithBlankSession() {
        Assertions.assertEquals("listOrders", orderController.welcome(mockModel, mockSession));
    }

    @Test
    void testWelcomeWithExistantSession() {
        Mockito.when(mockSession.getAttribute("orders")).thenReturn(1);
        Assertions.assertEquals("listOrders", orderController.welcome(mockModel, mockSession));
    }

    @Test
    void testDeleteOrder() {
        Assertions.assertEquals("redirect:/listOrders", orderController.deleteOrder(1, mockModel, mockSession));
    }

    @Test
    void testCreateOrder(){
        Assertions.assertEquals("createOrder", orderController.createOrder(mockModel));
    }

    @Test
    void testCreateOrderAndSaveWithoutErrors(){
        Mockito.when(mockForm.getAdrEt()).thenReturn(20.00);
        Mockito.when(mockForm.getLabel()).thenReturn("Label");
        Mockito.when(mockForm.getNotes()).thenReturn("Notes");
        Mockito.when(mockForm.getNumberOfDays()).thenReturn(100.00);
        Mockito.when(mockForm.getStatus()).thenReturn("En cours");
        Mockito.when(mockForm.getTva()).thenReturn(20.00);
        Mockito.when(mockForm.getType()).thenReturn("Forfait");
        Mockito.when(mockForm.getCustomerId()).thenReturn(2);
        Mockito.when(mockBindingResult.hasErrors()).thenReturn(false);

        Assertions.assertEquals("redirect:/listOrders", orderController.createOrderAndSave(mockSession, mockForm, mockBindingResult, mockModel));
    }

    @Test
    void testCreateOrderAndSaveWithErrors(){
        Mockito.when(mockBindingResult.hasErrors()).thenReturn(true);
        Assertions.assertEquals("redirect:/createOrder", orderController.createOrderAndSave(mockSession, mockForm, mockBindingResult, mockModel));
    }
}
