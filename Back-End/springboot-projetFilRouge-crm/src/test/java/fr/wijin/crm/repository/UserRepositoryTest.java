package fr.wijin.crm.repository;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import fr.wijin.crm.model.User;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@TestMethodOrder(OrderAnnotation.class)
@DisplayName("Test unitaire UserRepository")
public class UserRepositoryTest {
    
    static List<User> initListUsers = null;
    static User user = null;

    @Autowired
    private UserRepository userRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @BeforeEach
    void setup() {
        initListUsers = userRepository.findAll();
        user = initListUsers.get(0);
    }

    @Test
    @DisplayName("Test findByUsernameAndPassword")
    void testFindByUsernameAndPassword(){
        Assertions.assertEquals("toto", userRepository.findByUsernameAndPassword("toto", "$2a$12$KfFCPtUqv6InTXU4JOLcZuLYzqe3Nw4f61KZlakelYav3mP5JGg32").getUsername());
    }

}
