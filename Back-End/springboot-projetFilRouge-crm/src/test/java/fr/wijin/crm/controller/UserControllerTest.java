package fr.wijin.crm.controller;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import fr.wijin.crm.form.UserForm;
import fr.wijin.crm.model.Customer;
import fr.wijin.crm.model.User;
import fr.wijin.crm.repository.UserRepository;
import jakarta.servlet.http.HttpSession;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureWebMvc
public class UserControllerTest {
	
	@Autowired
    @InjectMocks
    private UserController userController;

    @Mock
    UserRepository mockUserRepository;

    @Mock
    HttpSession mockSession;

    @Mock
    Model mockModel;

    @Mock
    UserForm mockUserForm;

    @Mock
    BindingResult mockBindingresult;
    
    @Test
    void testLoadUser() {
        User user = new User(1, "toto", "$2a$12$KfFCPtUqv6InTXU4JOLcZuLYzqe3Nw4f61KZlakelYav3mP5JGg32", "toto.titi@toto.com", "ROLE_ADMIN");
        Mockito.when(mockUserRepository.findById(1)).thenReturn(Optional.of(user));
        Assertions.assertEquals("listUsers", userController.loadUser(1, mockSession));
    }

    @Test
    void testShowThemAll(){
        Mockito.when(mockSession.getAttribute("role")).thenReturn("ROLE_ADMIN");
        Assertions.assertEquals("listUsers", userController.showThemAll(mockModel, mockSession));

    }

    @Test
    void testShowFormmWithCustomerForm() {
        Assertions.assertEquals("createUser", userController.showForm(mockModel, mockUserForm));
    }

    @Test
    void testCreateUser(){
        Mockito.when(mockUserForm.getPassword()).thenReturn("ok");
        Mockito.when(mockUserForm.getMail()).thenReturn("ok");
        Mockito.when(mockUserForm.getGrants()).thenReturn("ROLE_ADMIN");
        Mockito.when(mockUserForm.getUsername()).thenReturn("ok");
        Assertions.assertEquals("redirect:/listUsers", userController.createUser(mockSession, mockUserForm, mockBindingresult, mockModel));
    }

    @Test
    void testDeleteUser() {
        Assertions.assertEquals("redirect:/listUsers", userController.deleteUser(1, mockModel, mockSession));
    }


}
