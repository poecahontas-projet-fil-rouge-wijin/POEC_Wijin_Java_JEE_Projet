package fr.wijin.crm.repository;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import org.springframework.test.context.junit.jupiter.SpringExtension;

import fr.wijin.crm.model.Customer;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@TestMethodOrder(OrderAnnotation.class)
@DisplayName("Test unitaire CustomerRepository")
public class CustomerRepositoryTest {

    static List<Customer> initListCustomer = null;
    static Customer customer = null;
    
    @Autowired
    private CustomerRepository customerRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @BeforeEach
    void setup() {
        initListCustomer = customerRepository.findAll();
        customer = initListCustomer.get(0);
        System.out.println(initListCustomer.size());
    }

    @Test
    @DisplayName("Test findAll")
    void testFindAll() {
        Assertions.assertEquals(4, customerRepository.findAll().size());
    }

    
    @Test
    @DisplayName("Test findByLastname")
    void testFindByLastname() {
        customer = customerRepository.findByLastname("JONES");
        Assertions.assertEquals("JONES", customer.getLastname());
    }

    @Test
    @DisplayName("Test findByActive")
    void testFindByActive() {
        int counter = 0;
        for (Customer customer : initListCustomer) {
            if(customer.isActive()) {
                counter++;
            }
        }
        Assertions.assertEquals(counter, customerRepository.findByActive(true).size());
    }    
}