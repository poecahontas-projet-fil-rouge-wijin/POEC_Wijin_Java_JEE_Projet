package fr.wijin.crm.form;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class OrderForm {

	@Id
	@GeneratedValue
	private Integer id;

	@NotNull
	@Size(max= 100)
	private String label;

	@NotNull
	@Min(0)
	private Double adrEt;

	@NotNull
	@Min(0)
	private Double numberOfDays;

	@NotNull
	@Min(0)
	private Double tva;

	@Size(max = 30)
	private String status;

	@Column(length = 100)
	private String type;

	@Size(max=200)
	private String notes;
	
	@Size(max=200)
	private String guarantee;

	@Override
	public String toString() {
		return "OrderForm [id=" + id + ", label=" + label + ", adrEt=" + adrEt + ", numberOfDays=" + numberOfDays
				+ ", tva=" + tva + ", status=" + status + ", type=" + type + ", notes=" + notes + ", guarantee="
				+ guarantee + ", estimate=" + estimate + ", customerId=" + customerId + "]";
	}

	@NotNull
	private Boolean estimate;

	// @NotNull
	private Integer customerId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Double getAdrEt() {
		return adrEt;
	}

	public void setAdrEt(Double adrEt) {
		this.adrEt = adrEt;
	}

	public Double getNumberOfDays() {
		return numberOfDays;
	}

	public void setNumberOfDays(Double numberOfDays) {
		this.numberOfDays = numberOfDays;
	}

	public Double getTva() {
		return tva;
	}

	public void setTva(Double tva) {
		this.tva = tva;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getGuarantee() {
		return guarantee;
	}

	public void setGuarantee(String guarantee) {
		this.guarantee = guarantee;
	}

	public Boolean getEstimate() {
		return estimate;
	}

	public void setEstimate(Boolean estimate) {
		this.estimate = estimate;
	}
	
}
