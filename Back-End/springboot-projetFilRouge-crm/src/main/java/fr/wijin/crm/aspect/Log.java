package fr.wijin.crm.aspect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Cette annotation personnalisée, @Log, est utilisée pour marquer les méthodes nécessitant une journalisation particulière.
 * Elle peut être utilisée en conjonction avec l'aspect LogAspect pour intercepter et enregistrer des informations spécifiques
 * lors de l'exécution de ces méthodes.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Log {
	
}