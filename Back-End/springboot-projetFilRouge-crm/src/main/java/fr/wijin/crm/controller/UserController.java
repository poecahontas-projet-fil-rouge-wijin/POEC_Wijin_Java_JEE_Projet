package fr.wijin.crm.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import jakarta.servlet.http.HttpSession;
import fr.wijin.crm.form.UserForm;
import fr.wijin.crm.model.User;
import fr.wijin.crm.repository.UserRepository;

/**
 * Contrôleur gérant les opérations liées aux utilisateurs dans l'application CRM.
 * Ce contrôleur est responsable du traitement des requêtes HTTP liées à la gestion des utilisateurs,
 * telles que l'affichage de la liste des utilisateurs, la création d'un nouvel utilisateur,
 * la suppression d'un utilisateur, etc.
 */
@Controller 
public class UserController {
	
public static final String SESSION_USERS = "users";
	
	public static final String PAGE_USERS_LIST = "listUsers";
	public static final String PAGE_USER_CREATE = "createUser";
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
    /**
     * Obtient un objet à partir de la session en utilisant la clé spécifiée.
     *
     * @param session La session HTTP dans laquelle rechercher l'objet.
     * @param key     La clé de l'objet dans la session.
     * @param <U>     Le type de l'objet.
     * @return L'objet trouvé dans la session ou null s'il n'est pas présent.
     */
	//@SuppressWarnings({ "unchecked", "hiding" })
	private <U> U getFromSession(HttpSession session, String key) {
		Object found = session.getAttribute(key);
		if (null != found) {
			return (U)found;
		}
		return null; 
	}
	
	/**
	 * Charge les détails d'un utilisateur spécifié par son identifiant et les stocke en session.
	 *
	 * @param userId L'identifiant de l'utilisateur à charger.
	 * @param session La session HTTP utilisée pour stocker des informations temporaires.
	 * @return Le nom de la vue à afficher après le chargement de l'utilisateur.
	 */
	@GetMapping(path = "/loadUser/{userId}")
	public String loadUser(
			@PathVariable(name = "userId") Integer userId,
			HttpSession session) {
		
		var user = userRepository.findById(userId)
				.orElse(null);
		
		Map<Integer, User> usersMap = new HashMap<>();
		usersMap.put(user.getId(), user);
		session.setAttribute(SESSION_USERS, usersMap);
		

		return "listUsers";
		
	}
	
    /**
     * Affiche la liste de tous les utilisateurs.
     *
     * @param model   Le modèle utilisé pour transmettre des données à la vue.
     * @param session La session HTTP utilisée pour stocker des informations temporaires.
     * @return Le nom de la vue à afficher pour la liste des utilisateurs.
     */
	@GetMapping("/listUsers")
	public String showThemAll(Model model, HttpSession session) {
		
		
			List<User> users = userRepository.findAll();
			System.out.println(users);
			
			Map<Integer, User> usersMap = new HashMap<>(); 
			users.forEach(user -> usersMap.put(user.getId(), user));
			System.out.println(usersMap);
			
			session.setAttribute(SESSION_USERS, usersMap);
			System.out.println(session.getAttribute(SESSION_USERS));
			model.addAttribute(SESSION_USERS, users);
			System.out.println(model.addAttribute(SESSION_USERS, users));
		
		return PAGE_USERS_LIST;
	}
	
    /**
     * Supprime un utilisateur spécifié par son identifiant.
     *
     * @param id      L'identifiant de l'utilisateur à supprimer.
     * @param model   Le modèle utilisé pour transmettre des données à la vue.
     * @param session La session HTTP utilisée pour stocker des informations temporaires.
     * @return Le nom de la vue à afficher après la suppression de l'utilisateur.
     */
	@GetMapping("/users/delete/{userId}")
	public String deleteUser(
			@PathVariable("userId") Integer id, Model model, HttpSession session){
		try {
			Optional<User> user = userRepository.findById(id);
			
		if (user.isPresent()) {
			
			userRepository.delete(user.get());

			Map<Integer, User> users = (HashMap<Integer, User>) session.getAttribute(SESSION_USERS);
			// Suppression de la commande de la Map
			users.remove(id);
			session.removeAttribute("error");
		}
	} catch (Exception e) {
		session.setAttribute("error", "Impossible de supprimer le client!");
	}
		return "redirect:/" + PAGE_USERS_LIST;
	}
	
    /**
     * Affiche le formulaire de création d'un nouvel utilisateur.
     *
     * @param model   Le modèle utilisé pour transmettre des données à la vue.
     * @param userForm Le formulaire utilisé pour saisir les informations de l'utilisateur.
     * @return Le nom de la vue à afficher pour le formulaire de création d'utilisateur.
     */
	@GetMapping("/createUser")
	public String showForm(Model model, UserForm userForm) {
		List<User> users = userRepository.findAll();
		model.addAttribute("userForm", new UserForm());
		model.addAttribute("customers", users);
		return PAGE_USER_CREATE;
	}
	
    /**
     * Crée un nouvel utilisateur en utilisant les informations fournies dans le formulaire.
     *
     * @param session      La session HTTP utilisée pour stocker des informations temporaires.
     * @param form         Le formulaire contenant les informations du nouvel utilisateur.
     * @param bindingResult Le résultat de la liaison qui contient les erreurs de validation.
     * @param model        Le modèle utilisé pour transmettre des données à la vue.
     * @return Le nom de la vue à afficher après la création de l'utilisateur.
     */
	@PostMapping("/createUser")
	public String createUser(HttpSession session, @ModelAttribute("userForm") @Validated UserForm form, BindingResult bindingResult, Model model) {
	
		
		if (!bindingResult.hasErrors()) {
			
		User user = new User();
		user.setUsername(form.getUsername());
		user.setPassword(passwordEncoder.encode(form.getPassword()));
		user.setMail(form.getMail());		
		user.setGrants(form.getGrants());
	
		
		userRepository.save(user);
		System.out.println(user);
		
		model.addAttribute("user", user);
		System.out.println(model.addAttribute("user", user));
		
		Map<Integer, User> users = (Map<Integer, User>) session.getAttribute(SESSION_USERS);
		
		if (users == null) {
			users = new HashMap<>();
		}
		 users.put(user.getId(), user);
		 
		 session.setAttribute(SESSION_USERS, users);
		 
		 System.out.println("*** Pas d'erreur : redirection vers /listUsers ***");
			return "redirect:/" + PAGE_USERS_LIST;
		} else {
			System.out.println("*** Erreur : redirection vers /createUsers ***");
			return "redirect:/" + PAGE_USER_CREATE;
		}
}
			
}