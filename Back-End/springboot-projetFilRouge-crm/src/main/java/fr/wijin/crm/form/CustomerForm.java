package fr.wijin.crm.form;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class CustomerForm {
	
	private String id;

	@NotNull
	@Size(min = 2, max = 100)
	private String firstname;

	@NotNull
	@Size(min = 2, max = 100)
	private String lastname;

	@NotNull
	@Size(min = 2, max = 200)
	private String company;

	@NotNull
	@Email
	@Size(min = 3, max = 255)
	private String mail;

	@NotNull(message = "phone est obligatoire")
	@Size(min = 10, max = 15)
	private String phone;

	@NotNull
	@Size(min = 10, max = 15)
	private String mobile;

	@Size(max = 255)
	private String notes;

	@NotNull
	private Boolean active;

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String toString() {
		return "Person(Firstname: " + this.firstname + ", Lastname: " + this.lastname + ", Company: " + this.company
				+ ", Mail: " + this.mail + ", Phone: " + this.phone + ", Mobile: " + this.mobile + ", Notes: "
				+ this.notes + ", Active : " + this.active + ")";
	}

}
