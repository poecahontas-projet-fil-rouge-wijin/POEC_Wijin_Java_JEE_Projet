package fr.wijin.crm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fr.wijin.crm.aspect.Log;

/**
 * Contrôleur gérant les opérations liées à l'authentification et aux erreurs d'accès.
 * Ce contrôleur est responsable du traitement des requêtes HTTP liées à l'authentification,
 * telles que l'affichage de la page de connexion, la gestion des erreurs d'authentification,
 * ainsi que la gestion des erreurs d'accès (403).
 */
@Controller
@Service
public class LoginController {

    /**
     * Affiche la page de connexion.
     *
     * @param error  Le paramètre indiquant s'il y a eu une erreur d'authentification.
     * @param model  Le modèle utilisé pour transmettre des données à la vue.
     * @return Le nom de la vue à afficher pour la page de connexion.
     */
	@GetMapping("/login")
	@Log
	public String login(
			@RequestParam(name="error", required = false) String error, 
			Model model) {
		if (error != null) {
			model.addAttribute("message", "Vous n'avez pas été identifié !");
		} else {
			model.addAttribute("message", "");
		}
		return "login";
	}
	
    /**
     * Affiche la page d'erreur 403 (accès non autorisé).
     *
     * @return Le nom de la vue à afficher pour la page d'erreur 403.
     */
	@GetMapping("/error/403")
	public String error403() {
		return "error/403";
	}
	
}
