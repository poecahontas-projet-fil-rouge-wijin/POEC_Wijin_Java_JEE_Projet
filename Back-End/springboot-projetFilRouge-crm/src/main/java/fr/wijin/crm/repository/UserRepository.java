package fr.wijin.crm.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.wijin.crm.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {
	
	/**
	 * Get a user by username
	 * @param username the username
	 * @return the user
	 */
	User findByUsername(String username);
	
	/**
	 * Get a user by username and password
	 * @param username the username
	 * @param password the password
	 * @return the user
	 */
	User findByUsernameAndPassword(String username, String password);

}
