package fr.wijin.crm.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;



/**
 * Cette classe représente un aspect pour la journalisation (logging) dans l'application.
 * Elle utilise AspectJ pour intercepter et gérer différents points d'exécution du code.
 */

@Aspect
@Component
public class LogAspect {
	
	Logger logger = LoggerFactory.getLogger(LogAspect.class);

    /**
     * Intercepte et exécute après l'exécution de méthodes dans le package fr.wijin.crm.controller.
     * @param joinPoint L'objet JoinPoint contenant des informations sur le point d'exécution.
     */
	@After("execution(* fr.wijin.crm.controller..*(..))")
	public void afterLogging(JoinPoint joinPoint) {
		System.out.println("\n*** After running loggingAdvice on method" + joinPoint.getSignature().getName());
		logger.info("*** After running logginAdvice on method" + joinPoint.getSignature().getName());
	}
	
//	@After("@annotation(fr.wijin.crm.controller)")
//	public void afterWithCustomAnnotation(JoinPoint joinPoint) {
//		System.out.println("\n*** Test after with custom Annotation" + joinPoint.getSignature().getName());
//	}
	
    /**
     * Intercepte et exécute après l'exécution de la méthode deleteCustomer dans le package fr.wijin.crm.controller.
     * @param joinPoint L'objet JoinPoint contenant des informations sur le point d'exécution.
     */
	@After("execution(* fr.wijin.crm.controller..deleteCustomer(*))")
	public void afterAdviceForDeleteCustomerMethod(JoinPoint joinPoint) {
		System.out.println("\n*** beforeAdviceForDeleteCustomerMethod()" + joinPoint.getSignature().getName());
	}
}