package fr.wijin.crm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

/**
 * Configuration de la sécurité web.
 * Cette classe déclare la configuration de la sécurité, notamment les règles d'accès aux différentes URLs,
 * les gestionnaires d'authentification, les services de détails d'utilisateurs, etc.
 */
@Configuration
@EnableWebSecurity
@EnableAspectJAutoProxy
@ComponentScan("fr.wijin.crm")
public class WebSecurity {

    /**
     * Définit la configuration de sécurité pour les différentes URL et les actions associées.
     *
     * @param http L'objet HttpSecurity utilisé pour configurer la sécurité HTTP.
     * @return Un objet SecurityFilterChain représentant la configuration de sécurité.
     * @throws Exception Si une exception survient lors de la configuration de la sécurité.
     */
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		http.csrf(c -> c.disable())
			.authorizeHttpRequests( auth -> auth	
			    .requestMatchers("/actuator/**").hasRole("ADMIN")

				.anyRequest().authenticated()
				
			  )
			.formLogin(
					//Customizer.withDefaults()
					form -> form.loginPage("/login")
						.defaultSuccessUrl("/listCustomers", true)
						.permitAll()
			)
			.logout( logout -> logout.invalidateHttpSession(true))
			.exceptionHandling(exception -> 
				exception.accessDeniedPage("/error/403"))
			;
		return http.build();
	}
	
    /**
     * Définit le gestionnaire d'authentification personnalisé.
     *
     * @param daoAuthenticationProvider Le fournisseur d'authentification DAO.
     * @return Un objet AuthenticationManager personnalisé.
     */
	@Bean
	// Sélection du Provider Manager (dans l'AuthenticationManager)
	public AuthenticationManager authenticationManager(
			DaoAuthenticationProvider daoAuthenticationProvider) {
		return new ProviderManager(daoAuthenticationProvider);
	}
	
    /**
     * Définit le fournisseur d'authentification DAO.
     *
     * @return Un objet DaoAuthenticationProvider configuré avec le service de détails d'utilisateurs et l'encodeur de mot de passe.
     */
	@Bean
	// Fourniture du référentiel d'identifiaction 
	public DaoAuthenticationProvider authenticationProvider() {
		var authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService());
		authProvider.setPasswordEncoder(passwordEncoder());
		
		return authProvider;
	}
	
    /**
     * Fournit le service de détails d'utilisateurs personnalisé.
     *
     * @return Un objet UserDetailsService configuré avec l'implémentation MonUserDetailsServiceImpl.
     */
	@Bean
	public UserDetailsService userDetailsService() {
		return new MonUserDetailsServiceImpl();
	}
	

    /**
     * Fournit un encodeur de mot de passe BCrypt.
     *
     * @return Un objet BCryptPasswordEncoder.
     */
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
}

