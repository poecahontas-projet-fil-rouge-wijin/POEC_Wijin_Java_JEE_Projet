package fr.wijin.crm.config;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import fr.wijin.crm.model.User;

/**
 * Cette classe implémente l'interface UserDetails de Spring Security pour représenter les détails d'un utilisateur.
 * Elle est utilisée dans le processus d'authentification pour fournir des informations sur l'utilisateur au système de sécurité.
 */
public class MonUserDetails implements UserDetails {

	private User user;
	
    /**
     * Constructeur de la classe MonUserDetails.
     *
     * @param user L'utilisateur pour lequel les détails sont créés.
     */
	public MonUserDetails(User user) {
		this.user = user;
	}
    /**
     * Retourne la liste des autorités (rôles) accordées à l'utilisateur.
     *
     * @return Une collection de GrantedAuthority représentant les rôles de l'utilisateur.
     */
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Collection<GrantedAuthority> liste =
				new ArrayList<>();
		liste.add(new SimpleGrantedAuthority(user.getGrants()));
		return liste;
	}

	@Override
	public String getPassword() {
		return user.getPassword();
	}

	@Override
	public String getUsername() {
		return user.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}