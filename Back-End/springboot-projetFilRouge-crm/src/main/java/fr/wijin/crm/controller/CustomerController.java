package fr.wijin.crm.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import jakarta.servlet.http.HttpSession;
import fr.wijin.crm.aspect.Log;
import fr.wijin.crm.form.CustomerForm;
import fr.wijin.crm.model.Customer;
import fr.wijin.crm.repository.CustomerRepository;

/**
 * Contrôleur gérant les opérations liées aux clients dans l'application CRM.
 * Ce contrôleur est responsable du traitement des requêtes HTTP liées à la gestion des clients,
 * telles que l'affichage de la liste des clients, la création d'un nouveau client, la suppression d'un client, etc.
 */
@Controller
@Service
//@PreAuthorize("hasRole('ADMIN', 'USER')")
public class CustomerController {

	@Autowired
	private CustomerRepository customerRepository;

	public static final String SESSION_CUSTOMERS = "customers";
	public static final String SESSION_CUSTOMER = "customer";

    /**
     * Charge un client spécifique en session à partir de son identifiant.
     *
     * @param customerId L'identifiant du client à charger.
     * @param session     La session HTTP dans laquelle stocker les informations du client.
     * @return Le nom de la vue à afficher après le chargement du client.
     */
	@GetMapping(path = "/loadCustomer/{customerId}")
	public String loadCustomer(
			@PathVariable(name = "customerId") Integer customerId,
			HttpSession session) {
		
		var customer = customerRepository.findById(customerId)
				.orElse(null);
		
		Map<Integer, Customer> clientsMap = new HashMap<>();
		clientsMap.put(customer.getId(), customer);
		session.setAttribute(SESSION_CUSTOMERS, clientsMap);

		return "listCustomers";
		
	}
	
    /**
     * Affiche la liste des clients.
     *
     * @param model   Le modèle utilisé pour transmettre des données à la vue.
     * @param session La session HTTP utilisée pour stocker des informations temporaires.
     * @return Le nom de la vue à afficher pour la liste des clients.
     */
	@GetMapping("/listCustomers")
	// @PreAuthorize("hasAuthority('ROLE_ADMIN')") // ROLE_ADMIN
	@PreAuthorize("hasRole('ADMIN', 'USER')") // ADMIN
	public String showForm(Model model, HttpSession session) {

		// On stocke le role dans la session depuis cette URL d'accueil
		if (session.getAttribute("role") == null) {
			var grants = (List<GrantedAuthority>) SecurityContextHolder.getContext()
				.getAuthentication().getAuthorities();
			session.setAttribute("role", grants.size() > 0 ? 
					grants.get(0).getAuthority() : "");
		}
		
		/*
		 * Si la Map des clients n'existe pas en session, alors l'utilisateur se
		 * connecte pour la première fois et il faut charger en session les informations
		 * contenues dans la BDD
		 */
		//if (session.getAttribute(SESSION_CUSTOMERS) == null) {
			// Récupération de la liste des clients existants et enregistrement en session
			List<Customer> customersList = customerRepository.findAll();
			Map<Integer, Customer> clientsMap = new HashMap<>();
			for (Customer customer : customersList) {
				clientsMap.put(customer.getId(), customer);
			}
			session.setAttribute(SESSION_CUSTOMERS, clientsMap);
			model.addAttribute("customers", customersList);
		//}

		return "listCustomers";
	}

    /**
     * Affiche le formulaire de création d'un nouveau client.
     *
     * @param customerForm Le formulaire utilisé pour créer un nouveau client.
     * @param model        Le modèle utilisé pour transmettre des données à la vue.
     * @return Le nom de la vue à afficher pour le formulaire de création de client.
     */
	@GetMapping("/createCustomer")
	public String showForm(CustomerForm customerForm, Model model) {
		model.addAttribute("customerForm", new CustomerForm());
		return "createCustomer";
	}
	
	/**
	 * Valide les informations du client et crée un nouveau client s'il n'y a pas d'erreurs.
	 *
	 * @param customerForm   Le formulaire contenant les informations du nouveau client.
	 * @param bindingResult  Le résultat de la liaison qui contient les erreurs de validation.
	 * @param model          Le modèle utilisé pour transmettre des données à la vue.
	 * @param session        La session HTTP utilisée pour stocker des informations temporaires.
	 * @return Le nom de la vue à afficher après la validation des informations du client.
	 */
	@PostMapping("/createCustomer")
	public String createCustomer(
			@ModelAttribute("customerForm") @Validated CustomerForm customerForm,
			BindingResult bindingResult, 
			Model model, 
			HttpSession session) {

		if (!bindingResult.hasErrors()) {
			Customer customer = new Customer();
			customer.setFirstname(customerForm.getFirstname());
			customer.setLastname(customerForm.getLastname());
			customer.setCompany(customerForm.getCompany());
			customer.setMail(customerForm.getMail());
			customer.setPhone(customerForm.getPhone());
			customer.setMobile(customerForm.getMobile());
			customer.setNotes(customerForm.getNotes());
			customer.setActive(customerForm.getActive());
			customerRepository.save(customer);

			// Ajout du bean Customer et de l'objet métier form à la requête
			model.addAttribute("customer", customer);
		

			// Pas d'erreur : récupération de la Map des clients dans la session
			Map<Integer, Customer> customers = (HashMap<Integer, Customer>) session.getAttribute(SESSION_CUSTOMERS);

			if (customers == null) {
				// Initialisation d'une Map si rien en session
				customers = new HashMap<>();
			}
			// Ajout du client courant dans la Map
			customers.put(customer.getId(), customer);
			// Repositionnement de la Map en session
			session.setAttribute(SESSION_CUSTOMERS, customers);

			System.out.println("*** Pas d'erreur : redirection vers /listCustomers ***");
			return "redirect:/listCustomers";
			// return "listCustomers"; // Possible mais pas de redirection (on ne passe pas
			// par le controller)
		} else {
			// Affichage du formulaire avec les erreurs
			System.out.println("*** Erreur : redirection vers /createCustomers ***");
			return "createCustomer";
		}

	}

	/**
	 * Supprime un client spécifié par son identifiant.
	 *
	 * @param id     L'identifiant du client à supprimer.
	 * @param model  Le modèle utilisé pour transmettre des données à la vue.
	 * @param session La session HTTP utilisée pour stocker des informations temporaires.
	 * @return Le nom de la vue à afficher après la suppression du client.
	 */
	//@GetMapping("/deleteCustomer")
	@GetMapping("/customers/delete/{customerId}")
	@PreAuthorize("hasAuthority('ADMIN')")
	@Log
	public String deleteCustomer(
			// @RequestParam("customerId") Integer id,
			@PathVariable("customerId") Integer id, Model model, HttpSession session) {
		try {
			Optional<Customer> customer = customerRepository.findById(id);
			if (customer.isPresent()) {
				customerRepository.delete(customer.get());

				Map<Integer, Customer> customers = (HashMap<Integer, Customer>) session.getAttribute(SESSION_CUSTOMERS);
				// Suppression du client de la Map
				customers.remove(id);
				session.removeAttribute("error");
			}
		} catch (Exception e) {
			session.setAttribute("error", "Impossible de supprimer le client!");
		}
		return "redirect:/listCustomers";
	}
	
//	/**
//	 * Affiche le formulaire de mise à jour des informations du client.
//	 *
//	 * @param id      L'identifiant du client à mettre à jour.
//	 * @param model   L'objet Model utilisé pour transmettre des données à la vue.
//	 * @param session L'objet HttpSession utilisé pour gérer la session.
//	 * @return La vue "updateCustomer" avec le formulaire de mise à jour des informations du client
//	 *         pré-rempli si le client est trouvé, sinon redirige vers la liste des clients.
//	 */
//	@GetMapping("/updateCustomer/{customerId}")
//	public String showUpForm(@PathVariable("customerId") Integer id,
//			Model model, HttpSession session) {
//		Customer custId = customerRepository.findById(id).orElse(null);
//		
//	    if (custId != null) {
//	        // Créez une nouvelle instance de CustomerForm et configurez ses propriétés avec les valeurs du Customer existant
//	        CustomerForm customerForm = new CustomerForm();
//	        customerForm.setFirstname(custId.getFirstname());
//	        customerForm.setLastname(custId.getLastname());
//	        customerForm.setCompany(custId.getCompany());
//	        customerForm.setMail(custId.getMail());
//	        customerForm.setPhone(custId.getPhone());
//	        customerForm.setMobile(custId.getMobile());
//	        customerForm.setNotes(custId.getNotes());
//	        customerForm.setActive(custId.getActive());
// 
//	        model.addAttribute("customerForm", customerForm);
//	        model.addAttribute("customer", custId);
//
//	        // Retournez la vue
//	        return "updateCustomer";
//	    } else {
//	        return "redirect:/listCustomers";
//	    }
//	}
//	
//	/**
//	 * Méthode de contrôleur pour mettre à jour les informations d'un client.
//	 *
//	 * @param customerForm   Le formulaire contenant les données mises à jour du client.
//	 * @param id             L'identifiant du client à mettre à jour.
//	 * @param bindingResult  Le résultat de la liaison des données avec le formulaire.
//	 * @param model          Le modèle utilisé pour transmettre des données à la vue.
//	 * @param session        La session HTTP pour stocker des informations entre les requêtes.
//	 * @return               La vue vers laquelle rediriger après la mise à jour du client.
//	 */
//	@PostMapping("/updateCustomer")
//	@PreAuthorize("hasAuthority('ADMIN')")
//	public String updateCustomer(
//			@ModelAttribute("customerForm") @Validated CustomerForm customerForm,
//			@PathVariable("customerId") Integer id,
//			BindingResult bindingResult, 
//			Model model, 
//			HttpSession session) {
//		
//		if (!bindingResult.hasErrors()) {
//			Customer custId = customerRepository.findById(id).orElse(null);
//			custId.setFirstname(customerForm.getFirstname());
//			custId.setLastname(customerForm.getLastname());
//			custId.setCompany(customerForm.getCompany());
//			custId.setMail(customerForm.getMail());
//			custId.setPhone(customerForm.getPhone());
//			custId.setMobile(customerForm.getMobile());
//			custId.setNotes(customerForm.getNotes());
//			custId.setActive(customerForm.getActive());
//			customerRepository.save(custId);
//			System.out.println("test");
//			// Ajout du bean Customer et de l'objet métier form à la requête
//			model.addAttribute("customer", custId);
//
//			// Pas d'erreur : récupération de la Map des clients dans la session
//			Map<Integer, Customer> customers = (HashMap<Integer, Customer>) session.getAttribute(SESSION_CUSTOMERS);
//
//			if (customers == null) {
//				// Initialisation d'une Map si rien en session
//				customers = new HashMap<>();
//			}
//			// Ajout du client courant dans la Map
//			customers.put(custId.getId(), custId);
//			// Repositionnement de la Map en session
//			session.setAttribute(SESSION_CUSTOMERS, customers);
//
//			System.out.println("*** Pas d'erreur : redirection vers /listCustomers ***");
//			return "redirect:/listCustomers";
//			// return "listCustomers"; // Possible mais pas de redirection (on ne passe pas
//			// par le controller)
//		} else {
//			// Affichage du formulaire avec les erreurs
//			System.out.println("*** Erreur : redirection vers /updateCustomers ***");
//			return "updateCustomer";
//		}
//
//	}

}

