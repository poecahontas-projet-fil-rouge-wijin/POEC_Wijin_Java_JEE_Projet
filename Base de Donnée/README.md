# Protocole d'installation & d'utilisation du conteneur PostgreSQL
- Créer un dossier "persistance" dans le dossier "Base de Donnée" à côté du dossier "Docker"

- Instruction docker pour créer l'image "bdd-pfr"
docker build -t bdd-pfr .

- Instruction docker pour créer le conteneur "bdd-pfr" en mode détaché
docker run -d --name bdd-pfr -e POSTGRES_USER={Sur le Discord} -e POSTGRES_PASSWORD={Sur le Discord} -e POSTGRES8DB=bdd-pfr -v ./persistance:/var/lib/postgresql/data -p 5432:5432 bdd-pfr

- Instruction docker pour redémarrer le conteneur "bdd-pfr"
docker start bdd-pfr

- Pour le monitoring avec DBeaver
    - Ajout d'une nouvelle connexion PostgreSQL
    - Remplacer le Host "Localhost" par l'IP donné par Portainer.
    - Port : 5432
    - Nom de la BDD : bdd-pfr
    - les identifiants de connexion sont épinglés dans la discution à 4 et utiliser ces identifiants lors du build.