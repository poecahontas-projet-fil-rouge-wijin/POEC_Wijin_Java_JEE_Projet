# POEC_Wijin_Java_JEE_Projet
 projet fil rouge de la POEC Java JEE dispensée par Wijin Academy

## Méthode pou faire un build production et CI/CD
 - Supprimer les Dossiers et leurs contenus suivants :
    - --Old
    - .vscode
    - Base de Donnée
    - bin
    - Cahier des charges
    - Front-End
    - target

 - Mettre les fichiers .gitignore, Jenkinsfile et pom.xml du dossier 'Config build' à la racine du projet en écrasant les fichiers existant.
 - Depuis le dosiier 'springboot-projetFilRouge-crm', déplacer le dossier 'src' et le ficheir 'mvnw' à la racine.
 - Supprimer le dossier 'Back-End'
